// src/main.rs

use std::env;
use std::path::Path;
use std::process::exit;

fn main() {
    // Make sure we have a filename argument
    let input_path = match env::args().skip(1).next() {
        Some(pathstr) => {
            pathstr
        },
        None => {
            println!("Usage: img_tool <filename>");
            exit(0);
        },
    };
    // Parse the optional rotation argument
    let rotation = match env::args().skip(2).next() {
        Some(rotation) => rotation,
        None => 0.to_string()
    };


    // Convert the input to a Path object
    let image_path = Path::new(&input_path);
    // Extract parts of the Path for re-use later
    let image_basepath = image_path.parent().unwrap();
    let image_filestem = image_path.file_stem().unwrap()
        .to_str().unwrap().to_owned();
    let image_fileext = image_path.extension().unwrap();
    
    // Build new path based on the parts of the original Path
    let newimage_path = image_basepath.join(image_filestem+"_rotated").with_extension(image_fileext);

    // Open the image
    let img = image::open(image_path).unwrap();

    // Rotate it by the given number of degrees, or 0 if none given
    let rotated : image::DynamicImage = match rotation.as_str() {
        "90"    => img.rotate90(),
        "180"   => img.rotate180(),
        "270"   => img.rotate270(),
        _       => img
    };
    rotated.save(newimage_path).unwrap();
}
